let tableauCartes = [
  "Images/chiens/6.webp",
  "Images/chiens/4.webp",
  "Images/chiens/8.webp",
  "Images/chiens/9.webp",
  "Images/chiens/10.webp",
  "Images/chiens/12.webp",
  "Images/chiens/4.webp",
  "Images/chiens/6.webp",
  "Images/chiens/8.webp",
  "Images/chiens/9.webp",
  "Images/chiens/10.webp",
  "Images/chiens/12.webp",
];

let imageBack = "Images/chiens/image.back_v2.png";
let image;
let carte1 = null;
let carte2 = null;
let nbPairesTrouvees = 0;

// Création et affichage du tableau
let plateau = document.getElementById('plateaudejeu');
let carte = plateau.getElementsByTagName("td");

for (let i = 0; i < carte.length; i++) {
  image = document.createElement('img');
  image.src = imageBack; // Afficher image de fond
  carte[i].appendChild(image);
  carte[i].setAttribute("idCarte", i); // Récupérer i de la carte
  carte[i].addEventListener("click", changerImage); 
}

// Changer image Front
function changerImage(event) {
  let id = event.currentTarget.getAttribute("idCarte");
  let imageFront = event.currentTarget.querySelector("img");
  
  // Vérif si la carte a déjà été trouvée
  if (imageFront.src.endsWith(imageBack) && (carte1 == null || carte2 == null)) {
    imageFront.src = tableauCartes[id];
    
  // Stocker la carte sélectionnée 
    if (carte1 == null) {
      carte1 = event.currentTarget;
    } else {
      carte2 = event.currentTarget;
      
      // Comparer les cartes sélectionnées
      if (carte1.querySelector("img").src == carte2.querySelector("img").src) {
        carte1 = null;
        carte2 = null;
        nbPairesTrouvees++; // Calcule le nombre de paires trouvées
        if (nbPairesTrouvees == tableauCartes.length / 2) { 
          alert("Quel talent ! Vous avez gagné");
        }
      } else {
        // Attendre un peu avant de retourner les cartes
        setTimeout(function() {
          carte1.querySelector("img").src = imageBack;
          carte2.querySelector("img").src = imageBack;
          carte1 = null;
          carte2 = null;
        }, 5000);
      }
    }
  }
}
